
const express = require("express");
const router = express.Router();

const taskController = require("./../controllers/userControllers")

router.post("/post-task", (req,res) => {
	taskController.postTask(req.body).then(result => res.send(result))
});

router.get("/", (req, res) => {
	taskController.getAllTasks(req.body).then(result => res.send(result))
});

router.get("/tasks/:id/", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(result => res.send(result))
	console.log(req)
});

router.put("/tasks/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id).then(result => res.send(result))
});


module.exports = router;
