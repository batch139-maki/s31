
const Task = require("./../models/Tasks.js");

module.exports.postTask = (reqBody) => {

	let taskName = reqBody.taskName;
	
	return Task.findOne({taskName: reqBody.taskName}).then( (result) =>{
		
		if(result != null){
			
			return `Task already exist`

		} else {

			let newTask = new Task ({
				taskName: reqBody.taskName,
			})
	
			return newTask.save().then( (result) => {
							
				return `New Task saved:${result}`
				
			})
		}
	}

)};

module.exports.getAllTasks = () => {

	return Task.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

module.exports.getSpecificTask = (id) => {
  	return Task.findById(id).then((result) => {
    	return result;
  });
};

module.exports.updateTask = (params) => {

	let updateStatus = {
		status: "complete"
	}
	
	return Task.findByIdAndUpdate(params, updateStatus, {new: true}).then((result) => {
    	return result;
  })
};
