const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 4000;


const taskRoutes = require("./routes/userRoutes.js");

mongoose.connect('mongodb+srv://machiavelli:*secret*@batch139.73pcg.mongodb.net/Task?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));


/*Middlewares*/
app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.use("/api/Tasks", taskRoutes);


app.listen(PORT, () => console.log(`Server running at port ${PORT}`))

