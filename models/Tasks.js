const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	taskName: {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	
})

module.exports = mongoose.model("Task", taskSchema);

